import compression from 'compression';
import cors from 'cors';
import dotenv from 'dotenv';
import express from 'express';
import { createClient } from 'redis';
import setAuthToken from './config/axios.js';
import configCloudinary from './config/configCloudinary.js';
import connectDB from './config/database.js';
import upload from './middleware/validateUpload.js';
import auth from './routers/auth.js';
import categories from './routers/categories.js';
import comments from './routers/comments.js';
import map from './routers/map.js';
import orders from './routers/orders.js';
import products from './routers/products.js';
import suppliers from './routers/suppliers.js';
import users from './routers/users.js';
import warehouses from './routers/warehouses.js';
// import os from 'os';
// import fs from 'fs';
// import PathFinder from 'geojson-path-finder';
// import { createRequire } from 'module'; // Bring in the ability to create the 'require' method
// const require = createRequire(import.meta.url); // construct the require method
// export const geojsonPoint = require('./route-network/ho-chi-minh-city_vietnam-P.json'); // use the require method
// const geojsonLineString = require('./route-network/ho-chi-minh-city_vietnam-LS.json'); // use the require method
dotenv.config();

// process.env.UV_THREADPOOL_SIZE = os.cpus().length > 6 ? 6 : 4;
// console.log('UV_THREADPOOL_SIZE', process.env.UV_THREADPOOL_SIZE);
const app = express();
app.use(cors());
app.use(express.json({ limit: '10mb' }));
app.use(express.urlencoded({ extended: true, limit: '10mb' }));
app.use(upload.single('file'));
app.use(
  compression({
    level: 6,
  })
);
connectDB();
configCloudinary();
setAuthToken();

export let redisClient;
(async () => {
  redisClient = createClient({
    url: process.env.REDIS_URL,
  });
  redisClient.on('error', (error) => console.error(`Redis ${error}`));
  redisClient.on('connect', () => {
    console.log('Redis connected');
  });
  await redisClient.connect();
})();

// const geojsonLineString = JSON.parse(
//   fs.readFileSync('./route-network/ho-chi-minh-city_vietnam-LS.json', 'utf8')
// ); // use the require method
// export const pathFinder = new PathFinder(geojsonLineString);

app.get('/', (_, res) => res.send('This is api of YumYum'));
app.use('/auth', auth);
app.use('/suppliers', suppliers);
app.use('/warehouses', warehouses);
app.use('/categories', categories);
app.use('/products', products);
app.use('/comments', comments);
app.use('/orders', orders);
app.use('/users', users);
app.use('/map', map);

const PORT = process.env.PORT || 5050;
app.listen(PORT, () => console.log(`Server is running on port ${PORT}`));
