import { redisClient } from '../index.js';

export const clearCache = async (cacheKey) => {
  // const keys = await redisClient.keys();
  const keys = await redisClient.sendCommand(['keys', '*']);
  keys.map(async (key) => {
    if (key.startsWith(`${cacheKey}`)) {
      await redisClient.del(key);
    }
  });
};
