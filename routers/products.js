import express from 'express';
import {
  addImage,
  createProduct,
  deleteImage,
  deleteProduct,
  getLatestProducts,
  getProductDetail,
  getProducts,
  getRelatedProducts,
  getTopDiscountProducts,
  getTopRatingProducts,
  updateProduct,
} from '../controllers/products.js';
import verifyToken from '../middleware/auth.js';
import authAdmin from '../middleware/authAdmin.js';
import redisCache from '../middleware/redisCache.js';

const router = express.Router();

// router.get('/', redisCache, getProducts);
// router.get('/latest', redisCache, getLatestProducts);
// router.get('/top-discount', redisCache, getTopDiscountProducts);
// router.get('/top-rating', redisCache, getTopRatingProducts);
// // router.get('/migrate-seo-url', migrateSeoUrlProducts);
// router.get('/related/:id', redisCache, getRelatedProducts);
// router.get('/:id', redisCache, getProductDetail);
// router.post('/', verifyToken, createProduct);
// router.put('/images/:id', verifyToken, addImage);
// router.put('/:id', verifyToken, updateProduct);
// router.delete('/images/:id/delete/:publicId', verifyToken, deleteImage);
// router.delete('/:id', verifyToken, deleteProduct);

router.get('/', redisCache, getProducts);
router.get('/latest', redisCache, getLatestProducts);
router.get('/top-discount', redisCache, getTopDiscountProducts);
router.get('/top-rating', redisCache, getTopRatingProducts);
// router.get('/migrate-seo-url', migrateSeoUrlProducts);
router.get('/related/:id', redisCache, getRelatedProducts);
router.get('/:id', redisCache, getProductDetail);
router.post('/', verifyToken, authAdmin, createProduct);
router.put('/images/:id', verifyToken, authAdmin, addImage);
router.put('/:id', verifyToken, authAdmin, updateProduct);
router.delete(
  '/images/:id/delete/:publicId',
  verifyToken,
  authAdmin,
  deleteImage
);
router.delete('/:id', verifyToken, authAdmin, deleteProduct);

export default router;
