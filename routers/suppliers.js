import express from 'express';
import {
  createSupplier,
  deleteSupplier,
  getSuppliers,
  updateSupplier,
} from '../controllers/suppliers.js';
import verifyToken from '../middleware/auth.js';
import authAdmin from '../middleware/authAdmin.js';
import redisCache from '../middleware/redisCache.js';

const router = express.Router();

// router.get('/', verifyToken, redisCache, getSuppliers);
// router.post('/', verifyToken, createSupplier);
// router.put('/:id', verifyToken, updateSupplier);
// router.delete('/:id', verifyToken, deleteSupplier);

router.get('/', verifyToken, authAdmin, redisCache, getSuppliers);
router.post('/', verifyToken, authAdmin, createSupplier);
router.put('/:id', verifyToken, authAdmin, updateSupplier);
router.delete('/:id', verifyToken, authAdmin, deleteSupplier);

export default router;
