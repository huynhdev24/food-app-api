import express from 'express';
import {
  addImage,
  deleteUser,
  getAllUsers,
  getUsers,
  updatePersonalInfo,
  updateUser,
} from '../controllers/users.js';
import verifyToken from '../middleware/auth.js';
import authAdmin, { authAdminChat } from '../middleware/authAdmin.js';
import redisCache from '../middleware/redisCache.js';

const router = express.Router();

// router.get('/migrate-user-coin', migrateUserCoin);
// router.get('/', verifyToken, authAdminChat, getAllUsers);
// router.get('/:role', verifyToken, redisCache, getUsers);
// router.put('/image/:id', verifyToken, addImage);
// router.put('/personal/:id', verifyToken, updatePersonalInfo);
// router.put('/:id', verifyToken, updateUser);
// router.delete('/:id', verifyToken, deleteUser);

router.get('/', verifyToken, authAdminChat, getAllUsers);
router.get('/:role', verifyToken, authAdmin, redisCache, getUsers);
router.put('/image/:id', verifyToken, addImage);
router.put('/personal/:id', verifyToken, updatePersonalInfo);
router.put('/:id', verifyToken, authAdmin, updateUser);
router.delete('/:id', verifyToken, authAdmin, deleteUser);
export default router;
