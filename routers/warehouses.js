import express from 'express';
import {
  createWarehouse,
  deleteWarehouse,
  getWarehouses,
  updateWarehouse,
} from '../controllers/warehouses.js';
import verifyToken from '../middleware/auth.js';
import authAdmin from '../middleware/authAdmin.js';
import redisCache from '../middleware/redisCache.js';

const router = express.Router();

// router.get('/', verifyToken, redisCache, getWarehouses);
// router.post('/', verifyToken, createWarehouse);
// router.put('/:id', verifyToken, updateWarehouse);
// router.delete('/:id', verifyToken, deleteWarehouse);

router.get('/', verifyToken, authAdmin, redisCache, getWarehouses);
router.post('/', verifyToken, authAdmin, createWarehouse);
router.put('/:id', verifyToken, authAdmin, updateWarehouse);
router.delete('/:id', verifyToken, authAdmin, deleteWarehouse);

export default router;
