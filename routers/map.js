import express from 'express';
import { getLatlng } from '../controllers/map.js';

const router = express.Router();

router.post('/latlng', getLatlng);

export default router;
