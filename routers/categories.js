import express from 'express';
import {
  addImage,
  createCategory,
  deleteCategory,
  getCategories,
  updateCategory,
} from '../controllers/categories.js';
import verifyToken from '../middleware/auth.js';
import authAdmin from '../middleware/authAdmin.js';
import redisCache from '../middleware/redisCache.js';

const router = express.Router();

// router.get('/', redisCache, getCategories);
// router.post('/', verifyToken, createCategory);
// router.put('/image/:id', verifyToken, addImage);
// router.put('/:id', verifyToken, updateCategory);
// router.delete('/:id', verifyToken, deleteCategory);

router.get('/', redisCache, getCategories);
router.post('/', verifyToken, authAdmin, createCategory);
router.put('/image/:id', verifyToken, authAdmin, addImage);
router.put('/:id', verifyToken, authAdmin, updateCategory);
router.delete('/:id', verifyToken, authAdmin, deleteCategory);

export default router;
