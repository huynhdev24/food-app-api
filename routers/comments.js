import express from 'express';
import {
  createComment,
  deleteComment,
  getAllComments,
  getProductComments,
  updateComment,
} from '../controllers/comments.js';
import verifyToken from '../middleware/auth.js';
import authAdmin from '../middleware/authAdmin.js';
import redisCache from '../middleware/redisCache.js';

const router = express.Router();

// router.get('/', verifyToken, redisCache, getAllComments);
// router.get('/:id', redisCache, getProductComments);
// router.post('/:id', verifyToken, createComment);
// router.put('/:id', verifyToken, updateComment);
// router.delete('/:id', verifyToken, deleteComment);

router.get('/', verifyToken, authAdmin, redisCache, getAllComments);
router.get('/:id', redisCache, getProductComments);
router.post('/:id', verifyToken, createComment);
router.put('/:id', verifyToken, authAdmin, updateComment);
router.delete('/:id', verifyToken, authAdmin, deleteComment);

export default router;
