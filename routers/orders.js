import express from 'express';
import { momoPayment } from '../controllers/orders/momo.orders.js';
import {
  cancelOrder,
  createOrder,
  getAllOrders,
  getUserOrder,
  getUserOrders,
  updateCheckout,
  updateOrder,
} from '../controllers/orders/orders.js';
import {
  createCheckout,
  refundStripe,
} from '../controllers/orders/stripe.orders.js';
import verifyToken from '../middleware/auth.js';
import authAdmin from '../middleware/authAdmin.js';
import redisCache from '../middleware/redisCache.js';

const router = express.Router();

// router.get('/', verifyToken, redisCache, getAllOrders);
// router.get('/user-orders', verifyToken, getUserOrders);
// router.get('/user-order/:id', verifyToken, getUserOrder);
// router.get('/route/:id', verifyToken, getOrderRoute);
// router.post('/', verifyToken, createOrder);
// router.post('/create-checkout-session', verifyToken, createCheckout);
// router.post('/momo-payment', verifyToken, momoPayment);
// router.put('/update-checkout', verifyToken, updateCheckout);
// router.put('/cancel-order/:id', verifyToken, cancelOrder);
// router.put('/refund/stripe/:id', verifyToken, refundStripe);
// router.put('/:id', verifyToken, updateOrder);

router.get('/', verifyToken, authAdmin, redisCache, getAllOrders);
router.get('/user-orders', verifyToken, getUserOrders);
router.get('/user-order/:id', verifyToken, getUserOrder);
// router.get('/route/:id', verifyToken, getOrderRoute);
router.post('/', verifyToken, createOrder);
router.post('/create-checkout-session', verifyToken, createCheckout);
router.post('/momo-payment', verifyToken, momoPayment);
router.put('/update-checkout', verifyToken, updateCheckout);
router.put('/cancel-order/:id', verifyToken, cancelOrder);
router.put('/refund/stripe/:id', verifyToken, refundStripe);
router.put('/:id', verifyToken, authAdmin, updateOrder);

export default router;
