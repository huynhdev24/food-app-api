import { CategoryModel } from '@nhuthuynh.phu/yum-yum-common/dist/category/model.js';
import { WarehouseModel } from '@nhuthuynh.phu/yum-yum-common/dist/warehouse/model.js';
import { redisClient } from '../index.js';
import { clearCache } from '../utils/clearCache.js';

export const getWarehouses = async (req, res) => {
  try {
    const warehouses = await WarehouseModel.find().sort({ createdAt: -1 });
    const result = { success: true, warehouses };
    await redisClient.set(req.originalUrl, JSON.stringify(result), {
      EX: 60 * 60 * 24 * 7,
    });
    res.status(200).json(result);
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

export const createWarehouse = async (req, res) => {
  const { name, phone, address } = req.body;
  if (!name || !phone || !address)
    return res.status(400).json({
      success: false,
      message: 'Vui lòng điền đầy đủ thông tin',
    });

  try {
    const warehouseCodeExisted = await WarehouseModel.findOne({ name });
    if (warehouseCodeExisted)
      return res
        .status(400)
        .json({ success: false, message: 'Tên Kho hàng đã tồn tại' });

    const newWarehouse = req.body;
    let warehouse = new WarehouseModel(newWarehouse);
    await warehouse.save();
    await clearCache('/warehouses');
    res.status(200).json({
      success: true,
      message: 'Tạo Kho hàng mới thành công!',
      warehouse,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

export const updateWarehouse = async (req, res) => {
  const { name, phone, address } = req.body;
  if (!name || !phone || !address)
    return res.status(400).json({
      success: false,
      message: 'Vui lòng điền đầy đủ thông tin',
    });

  try {
    const warehouseCodeExisted = await WarehouseModel.findOne({
      _id: { $ne: req.params.id },
      name,
    });
    if (warehouseCodeExisted)
      return res
        .status(400)
        .json({ success: false, message: 'Tên Kho hàng đã tồn tại' });

    const updateWarehouse = req.body;
    const warehouse = await WarehouseModel.findOneAndUpdate(
      { _id: req.params.id },
      updateWarehouse,
      { new: true }
    );

    if (!warehouse)
      return res
        .status(404)
        .json({ success: false, message: 'Không tìm thấy Kho hàng' });
    await clearCache('/warehouses');
    res.status(200).json({
      success: true,
      message: 'Cập nhật Kho hàng thành công!',
      warehouse,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

export const deleteWarehouse = async (req, res) => {
  try {
    const category = await CategoryModel.findOne({
      warehouseId: req.params.id,
    });
    if (category)
      return res.status(400).json({
        success: false,
        message: 'Không thể xoá vì đã tồn tại loại sản phẩm',
      });

    const warehouse = await WarehouseModel.findOneAndDelete({
      _id: req.params.id,
    });
    if (!warehouse)
      return res
        .status(404)
        .json({ success: false, message: 'Không tìm thấy Kho hàng' });
    await clearCache('/warehouses');
    res
      .status(200)
      .json({ success: true, message: 'Xoá thành công!', warehouse });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};
