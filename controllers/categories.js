import { CategoryModel } from '@nhuthuynh.phu/yum-yum-common/dist/category/model.js';
import { ProductModel } from '@nhuthuynh.phu/yum-yum-common/dist/product/model.js';
import { v2 as cloudinary } from 'cloudinary';
import fs from 'fs';
import { redisClient } from '../index.js';
import { clearCache } from '../utils/clearCache.js';

export const getCategories = async (req, res) => {
  try {
    const categories = await CategoryModel.find()
      .sort({ createdAt: -1 })
      .populate([
        {
          path: 'warehouseId',
          select: ['id', 'name'],
        },
        {
          path: 'supplierId',
          select: ['id', 'name'],
        },
      ]);
    const result = { success: true, categories };
    await redisClient.set(req.originalUrl, JSON.stringify(result), {
      EX: 60 * 60 * 24 * 7,
    });
    res.status(200).json(result);
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

export const createCategory = async (req, res) => {
  const { name, supplierId, warehouseId } = req.body;
  if (!name || !supplierId || !warehouseId)
    return res.status(400).json({
      success: false,
      message: 'Vui lòng điền đầy đủ thông tin',
    });

  try {
    const categoryCodeExisted = await CategoryModel.findOne({ name });
    if (categoryCodeExisted)
      return res
        .status(400)
        .json({ success: false, message: 'Tên Loại sản phẩm đã tồn tại' });

    const newCategory = req.body;
    let category = new CategoryModel(newCategory);
    await category.save();
    category = await category.populate([
      {
        path: 'warehouseId',
        select: ['id', 'name'],
      },
      {
        path: 'supplierId',
        select: ['id', 'name'],
      },
    ]);
    await clearCache('/categories');
    res.status(200).json({
      success: true,
      message: 'Tạo Loại sản phẩm mới thành công!',
      category,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

export const updateCategory = async (req, res) => {
  const { name, supplierId, warehouseId } = req.body;
  if (!name || !supplierId || !warehouseId)
    return res.status(400).json({
      success: false,
      message: 'Vui lòng điền đầy đủ thông tin',
    });

  try {
    const categoryCodeExisted = await CategoryModel.findOne({
      _id: { $ne: req.params.id },
      name,
    });
    if (categoryCodeExisted)
      return res
        .status(400)
        .json({ success: false, message: 'Tên Loại sản phẩm đã tồn tại' });

    const updateCategory = req.body;
    const category = await CategoryModel.findOneAndUpdate(
      { _id: req.params.id },
      updateCategory,
      { new: true }
    ).populate([
      {
        path: 'warehouseId',
        select: ['id', 'name'],
      },
      {
        path: 'supplierId',
        select: ['id', 'name'],
      },
    ]);

    if (!category)
      return res
        .status(404)
        .json({ success: false, message: 'Không tìm thấy Loại sản phẩm' });
    await clearCache('/categories');
    res.status(200).json({
      success: true,
      message: 'Cập nhật Loại sản phẩm thành công!',
      category,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

export const deleteCategory = async (req, res) => {
  try {
    const product = await ProductModel.findOne({ categoryId: req.params.id });
    if (product)
      return res.status(400).json({
        success: false,
        message: 'Không thể xoá vì đã tồn tại sản phẩm',
      });

    const category = await CategoryModel.findOneAndDelete({
      _id: req.params.id,
    });
    if (!category)
      return res
        .status(404)
        .json({ success: false, message: 'Không tìm thấy Loại sản phẩm' });
    await clearCache('/categories');
    res
      .status(200)
      .json({ success: true, message: 'Xoá thành công!', category });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

export const addImage = async (req, res) => {
  try {
    const categoryExisted = await CategoryModel.findById(req.params.id);
    if (!categoryExisted)
      return res
        .status(404)
        .json({ success: false, message: 'Không tìm thấy Loại sản phẩm' });
    if (categoryExisted?.image?.public_id)
      await cloudinary.uploader.destroy(categoryExisted.image.public_id);

    const eager_options = {
      width: 770,
      height: 627,
      crop: 'fill',
      format: 'png',
    };
    const result = await cloudinary.uploader.upload(
      req.file.path,
      eager_options
    );
    fs.unlinkSync(req.file.path);

    const category = await CategoryModel.findOneAndUpdate(
      { _id: req.params.id },
      {
        image: { imageUrl: result.url, public_id: result.public_id },
      },
      { new: true }
    ).populate([
      {
        path: 'warehouseId',
        select: ['id', 'name'],
      },
      {
        path: 'supplierId',
        select: ['id', 'name'],
      },
    ]);
    await clearCache('/categories');
    res.status(200).json({
      success: true,
      message: 'Cập nhật loại sản phẩm thành công!',
      category,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};
