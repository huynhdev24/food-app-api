import { CommentModel } from '@nhuthuynh.phu/yum-yum-common/dist/comment/model.js';
import { OrderModel } from '@nhuthuynh.phu/yum-yum-common/dist/order/model.js';
import { UserModel } from '@nhuthuynh.phu/yum-yum-common/dist/user/model.js';
import { v2 as cloudinary } from 'cloudinary';
import fs from 'fs';
import { redisClient } from '../index.js';
import { clearCache } from '../utils/clearCache.js';

export const getUsers = async (req, res) => {
  try {
    const users = await UserModel.find({
      role: req.params.role.toUpperCase(),
    })
      .sort({ createdAt: -1 })
      .select('-password');
    const result = { success: true, users };
    await redisClient.set(req.originalUrl, JSON.stringify(result), {
      EX: 60 * 60 * 24 * 7,
    });
    res.status(200).json(result);
  } catch (error) {
    console.log(error);
    return res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

export const getAllUsers = async (req, res) => {
  try {
    const users = await UserModel.find();
    const result = { success: true, users };
    res.status(200).json(result);
  } catch (error) {
    console.log(error);
    return res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

export const updateUser = async (req, res) => {
  const { role, isBlock } = req.body;
  if (!role && isBlock === null && isBlock === undefined)
    return res
      .status(400)
      .json({ success: false, message: 'Vui lòng điền đầy đủ thông tin' });

  try {
    const user = await UserModel.findOneAndUpdate(
      { _id: req.params.id },
      { role, isBlock },
      { new: true, omitUndefined: true }
    ).select('-password');
    if (!user)
      return res
        .status(404)
        .json({ success: false, message: 'Tài khoản không tồn tại' });
    await clearCache('/users');
    res
      .status(200)
      .json({ success: true, message: 'Cập nhật tài khoản thành công', user });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

export const deleteUser = async (req, res) => {
  try {
    const [user] = await Promise.all([
      UserModel.findOneAndDelete({ _id: req.params.id }).select('-password'),
      CommentModel.deleteMany({ userId: req.params.id }),
      OrderModel.deleteMany({ userId: req.params.id }),
    ]);
    if (!user)
      return res
        .status(404)
        .json({ success: false, message: 'Tài khoản không tồn tại' });
    await clearCache('/users');
    res
      .status(200)
      .json({ success: true, message: 'Xóa tài khoản thành công', user });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

export const updatePersonalInfo = async (req, res) => {
  const {
    fullName,
    address,
    phone,
    wardCode,
    districtId,
    latlng,
    isIgnoreCheck = false,
  } = req.body;
  if ((!fullName || !address || !phone) && !isIgnoreCheck)
    return res
      .status(400)
      .json({ success: false, message: 'Vui lòng điền đầy đủ thông tin' });

  try {
    const phoneNumberExist = await UserModel.findOne({
      phone,
      _id: { $ne: req.params.id },
    });
    if (phoneNumberExist)
      return res
        .status(400)
        .json({ success: false, message: 'Số điện thoại đã tồn tại' });

    const user = await UserModel.findOneAndUpdate(
      { _id: req.params.id },
      { fullName, address, phone, wardCode, districtId, latlng },
      { new: true, omitUndefined: true }
    ).select('-password');
    if (!user)
      return res
        .status(404)
        .json({ success: false, message: 'Tài khoản không tồn tại' });
    await clearCache('/users');
    res
      .status(200)
      .json({ success: true, message: 'Cập nhật tài khoản thành công', user });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

export const addImage = async (req, res) => {
  try {
    const userExisted = await UserModel.findById(req.params.id);
    if (!userExisted)
      return res
        .status(404)
        .json({ success: false, message: 'Không tìm thấy Tài khoản' });
    if (userExisted?.avatar?.public_id)
      await cloudinary.uploader.destroy(userExisted.avatar.public_id);

    const eager_options = {
      width: 770,
      height: 627,
      crop: 'fill',
      format: 'png',
    };
    const result = await cloudinary.uploader.upload(
      req.file.path,
      eager_options
    );
    fs.unlinkSync(req.file.path);

    const user = await UserModel.findOneAndUpdate(
      { _id: req.params.id },
      {
        avatar: { imageUrl: result.url, public_id: result.public_id },
      },
      { new: true }
    ).select('-password');
    await clearCache('/users');
    res.status(200).json({
      success: true,
      message: 'Cập nhật Tài khoản thành công!',
      user,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

// export const migrateUserCoin = async (_req, res) => {
//   try {
//     const products = await UserModel.updateMany({}, { coin: 0 }, { new: true });
//     return res.status(200).json({ success: true, products });
//   } catch (error) {
//     console.log(error);
//     return res.status(200).json({ success: false, message: 'Lỗi máy chủ' });
//   }
// };
