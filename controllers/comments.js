import { CommentTypeEnum } from '@nhuthuynh.phu/yum-yum-common/dist/comment/enums.js';
import { CommentModel } from '@nhuthuynh.phu/yum-yum-common/dist/comment/model.js';
import { ProductModel } from '@nhuthuynh.phu/yum-yum-common/dist/product/model.js';
import axios from 'axios';
import { apiNotificationUrl } from '../constants/index.js';
import { redisClient } from '../index.js';
import { clearCache } from '../utils/clearCache.js';

export const getAllComments = async (req, res) => {
  try {
    const comments = await CommentModel.find({
      type: { $ne: CommentTypeEnum.REPLY_COMMENT },
    })
      .sort({ createdAt: -1 })
      .populate([
        {
          path: 'userId',
          select: ['id', 'fullName', 'avatar'],
        },
        {
          path: 'productId',
          select: ['id', 'name'],
        },
      ]);
    const result = { success: true, comments };
    await redisClient.set(req.originalUrl, JSON.stringify(result), {
      EX: 60 * 60 * 24 * 7,
    });
    res.status(200).json(result);
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

export const getProductComments = async (req, res) => {
  try {
    const comments = await CommentModel.find({
      productId: req.params.id,
      type: { $ne: CommentTypeEnum.REPLY_COMMENT },
    })
      .sort({ createdAt: -1 })
      .populate([
        {
          path: 'userId',
          select: ['id', 'fullName', 'avatar', 'role'],
        },
        {
          path: 'replyCommentId',
          select: ['id', 'content', 'createdAt'],
          populate: {
            path: 'userId',
            select: ['id', 'fullName', 'avatar', 'role'],
          },
        },
      ]);
    const result = { success: true, comments };
    await redisClient.set(req.originalUrl, JSON.stringify(result), {
      EX: 60 * 60 * 24 * 7,
    });
    res.status(200).json(result);
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

export const createComment = async (req, res) => {
  const { content, replyCommentId } = req.body;
  if (!content)
    return res.status(400).json({
      success: false,
      message: 'Vui lòng điền đầy đủ thông tin',
    });

  try {
    const [product, comments] = await Promise.all([
      ProductModel.findById(req.params.id),
      CommentModel.find({
        productId: req.params.id,
        type: { $ne: CommentTypeEnum.REPLY_COMMENT },
      }),
    ]);
    if (!product)
      return res
        .status(404)
        .json({ success: false, message: 'Không tìm thấy sản phẩm' });

    let comment;
    if (!replyCommentId) {
      comment = new CommentModel({
        content,
        rating: req.body.rating,
        productId: req.params.id,
        userId: req.userId,
      });
      await comment.save();
      comment = await comment.populate([
        {
          path: 'userId',
          select: ['id', 'fullName', 'avatar', 'role'],
        },
      ]);
      let sum = 0;
      if (comments.length > 0) {
        comments.forEach((commentItem) => (sum += commentItem.rating));
      }
      sum += comment.rating;
      await ProductModel.findOneAndUpdate(
        { _id: req.params.id },
        { rating: (sum * 1.0) / (comments.length + 1) },
        { new: true }
      );
    } else {
      const replyComment = new CommentModel({
        content,
        productId: req.params.id,
        userId: req.userId,
        type: CommentTypeEnum.REPLY_COMMENT,
      });
      await replyComment.save();
      comment = await CommentModel.findOneAndUpdate(
        { _id: replyCommentId },
        { replyCommentId: replyComment._id },
        { new: true }
      ).populate([
        {
          path: 'userId',
          select: ['id', 'fullName', 'avatar', 'role'],
        },
        {
          path: 'replyCommentId',
          select: ['id', 'content', 'createdAt'],
          populate: {
            path: 'userId',
            select: ['id', 'fullName', 'avatar', 'role'],
          },
        },
      ]);

      // mailer
      axios.post(`${apiNotificationUrl}/comments`, {
        userId: req.userId,
        productName: product.name,
        productSeoUrl: product.seoUrl,
      });
    }
    await clearCache('/comments');
    res
      .status(200)
      .json({ success: true, message: 'Tạo bình luận thành công!', comment });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

export const updateComment = async (req, res) => {
  let { content, rating, productId } = req.body;
  if (!content || !rating || !productId)
    return res.status(400).json({
      success: false,
      message: 'Vui lòng điền đầy đủ thông tin',
    });
  rating = parseInt(rating);

  try {
    const [comment, comments] = await Promise.all([
      CommentModel.findOneAndUpdate(
        { _id: req.params.id },
        { content, rating: parseInt(rating) },
        {
          new: true,
        }
      ).populate([
        {
          path: 'userId',
          select: ['id', 'fullName', 'avatar'],
        },
        {
          path: 'productId',
          select: ['id', 'name'],
        },
      ]),
      CommentModel.find({
        productId,
        type: { $ne: CommentTypeEnum.REPLY_COMMENT },
      }),
    ]);
    if (!comment)
      return res
        .status(404)
        .json({ success: false, message: 'Không tìm thấy bình luận' });

    let sum = 0;
    comments.forEach((commentItem) => {
      if (commentItem._id !== comment._id) {
        sum += commentItem.rating;
      } else {
        sum += comment.rating;
      }
    });
    await Promise.all([
      ProductModel.findOneAndUpdate(
        { _id: productId },
        { rating: (sum * 1.0) / comments.length },
        { new: true }
      ),
      clearCache('/comments'),
    ]);
    res.status(200).json({
      success: true,
      message: 'Cập nhật bình luận thành công!',
      comment,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

export const deleteComment = async (req, res) => {
  try {
    const comment = await CommentModel.findOneAndDelete({
      _id: req.params.id,
    });
    if (!comment)
      return res
        .status(404)
        .json({ success: false, message: 'Không tìm thấy bình luận' });
    const [comments] = await Promise.all([
      CommentModel.find({
        productId: comment.productId,
        type: { $ne: CommentTypeEnum.REPLY_COMMENT },
      }),
      CommentModel.findOneAndDelete({
        _id: comment.replyCommentId,
      }),
    ]);
    let sum = 0;
    if (comments.length > 0) {
      comments.forEach((commentItem) => {
        sum += commentItem.rating;
      });
    }
    await Promise.all([
      ProductModel.findOneAndUpdate(
        { _id: comment.productId },
        {
          rating:
            parseInt(sum) != parseInt(0) ? (sum * 1.0) / comments.length : 3,
        },
        { new: true }
      ),
      clearCache('/comments'),
    ]);

    res
      .status(200)
      .json({ success: true, message: 'Xoá thành công!', comment });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};
