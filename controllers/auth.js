import { RoleUserEnum } from '@nhuthuynh.phu/yum-yum-common/dist/user/enums.js';
import { UserModel } from '@nhuthuynh.phu/yum-yum-common/dist/user/model.js';
import { VerifyUserModel } from '@nhuthuynh.phu/yum-yum-common/dist/verifyUser/model.js';
import argon2 from 'argon2';
import axios from 'axios';
import { v2 as cloudinary } from 'cloudinary';
import { OAuth2Client } from 'google-auth-library';
import jwt from 'jsonwebtoken';
import { apiChatUrl, apiNotificationUrl } from '../constants/index.js';

export const getUser = async (req, res) => {
  try {
    const user = await UserModel.findById(req.userId).select('-password');
    if (!user)
      return res
        .status(404)
        .json({ success: false, message: 'Tên người dùng không tồn tại' });
    res.status(200).json({ success: true, user });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: 'Lấy thông tin thất bại' });
  }
};

export const register = async (req, res) => {
  const { username, email, password, fullName, phone } = req.body;
  if (!username || !email || !password || !fullName || !phone)
    return res
      .status(400)
      .json({ success: false, message: 'Vui lòng điền đầy đủ thông tin!' });
  if (username.includes(' '))
    return res.status(400).json({
      success: false,
      message: 'Tên người dùng không được phép có khoảng trắng',
    });

  try {
    const [usernameOrEmailOrPhoneExisted, hashedPassword] = await Promise.all([
      UserModel.findOne({ $or: [{ username }, { email }, { phone }] }),
      argon2.hash(password),
      VerifyUserModel.findOneAndDelete({ email }),
    ]);
    if (usernameOrEmailOrPhoneExisted?.username?.includes(username))
      return res.status(400).json({
        success: false,
        message: 'Tên người dùng đã tồn tại',
      });
    if (usernameOrEmailOrPhoneExisted?.email?.includes(email))
      return res.status(400).json({
        success: false,
        message: 'Email đã tồn tại',
      });
    if (usernameOrEmailOrPhoneExisted?.phone?.includes(phone))
      return res.status(400).json({
        success: false,
        message: 'Số điện thoại đã tồn tại',
      });

    const newUser = new VerifyUserModel({
      username,
      email,
      fullName,
      password: hashedPassword,
      phone,
    });
    await newUser.save();

    const accessToken = jwt.sign(
      { userId: newUser._id },
      process.env.ACCESS_TOKEN_SECRET,
      { expiresIn: '10m' }
    );

    // mailer
    axios.post(`${apiNotificationUrl}/auth/register`, {
      userId: newUser._id,
      email,
      accessToken,
      username,
    });

    res.status(200).json({ success: true, message: 'Đăng ký thành công!' });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: 'Đăng ký thất bại' });
  }
};

export const activate = async (req, res) => {
  const { token, id } = req.body;
  if (!token || !id)
    return res
      .status(400)
      .json({ success: false, message: 'Thiếu token hoặc ID' });

  try {
    const decoded = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);
    if (decoded.userId !== id)
      return res
        .status(400)
        .json({ success: false, message: 'Token hoặc ID không hợp lệ' });
    const user = await VerifyUserModel.findOneAndDelete({ _id: id });
    if (!user)
      return res
        .status(404)
        .json({ success: false, message: 'Người dùng không tồn tại' });

    const { username, email, password, fullName, phone } = user;
    const newUser = new UserModel({
      username,
      email,
      password,
      fullName,
      phone,
    });
    await newUser.save();

    // mailer
    axios.post(`${apiNotificationUrl}/auth/activate`, {
      userId: newUser._id,
      email,
    });
    // chat
    migrateUserToChat(newUser);

    res
      .status(200)
      .json({ success: true, message: 'Kích hoạt tài khoản thành công!' });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: 'Token không hợp lệ hoặc đã hết hạn' });
  }
};

export const login = async (req, res) => {
  const { username, password } = req.body;
  if (!username)
    return res
      .status(400)
      .json({ success: false, message: 'Thiếu tên người dùng' });
  if (!password)
    return res.status(400).json({ success: false, message: 'Thiếu mật khẩu' });

  try {
    const user = await UserModel.findOne(
      username.includes('@') ? { email: username } : { username: username }
    );
    if (!user)
      return res
        .status(404)
        .json({ success: false, message: 'Người dùng không tồn tại' });
    const passwordValid = await argon2.verify(user.password, password);
    if (!passwordValid)
      return res.status(400).json({ success: false, message: 'Sai mật khẩu' });

    const accessToken = jwt.sign(
      { userId: user._id },
      process.env.ACCESS_TOKEN_SECRET,
      { expiresIn: '7d' }
    );
    user.password = undefined;
    res.status(200).json({ success: true, user, accessToken });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: 'Đăng nhập thất bại' });
  }
};

export const sendMailResetPassword = async (req, res) => {
  const { email } = req.body;
  if (!email)
    return res
      .status(400)
      .json({ success: false, message: 'Vui lòng điền địa chỉ email!' });

  try {
    const verifyUserEmail = await UserModel.findOne({ email });
    if (!verifyUserEmail)
      return res
        .status(404)
        .json({ success: false, message: 'Không tìm thấy địa chỉ email' });
    const accessToken = jwt.sign(
      { userId: verifyUserEmail._id },
      process.env.ACCESS_TOKEN_SECRET,
      {
        expiresIn: '10m',
      }
    );

    // mailer
    axios.post(`${apiNotificationUrl}/auth/send-mail-reset-password`, {
      userId: verifyUserEmail._id,
      email,
      accessToken,
    });

    res
      .status(200)
      .json({ success: true, message: `Đã gửi email xác nhận đến ${email}` });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: 'Gửi email thất bại, hãy thử lại' });
  }
};

export const resetPassword = async (req, res) => {
  const { token, id, password } = req.body;
  if (!token || !id || !password)
    return res
      .status(400)
      .json({ success: false, message: 'Vui lòng điền đày đủ thông tin!' });

  try {
    const decoded = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);
    if (decoded.userId !== id)
      return res.status(400).json({
        success: false,
        message: 'Token không hợp lệ hoặc đã hết hạn',
      });
    const hashedPassword = await argon2.hash(password);
    const user = await UserModel.findOneAndUpdate(
      { _id: id },
      { password: hashedPassword },
      { new: true }
    );
    if (!user)
      return res
        .status(404)
        .json({ success: false, message: 'Người dùng không tồn tại' });

    axios.post(`${apiNotificationUrl}/auth/reset-password`, {
      userId: user._id,
      email: user.email,
    });
    // chat
    migrateUserToChat(user);

    res
      .status(200)
      .json({ success: true, message: 'Đặt lại mật khẩu thành công!' });
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      success: false,
      message: 'Đặt lại mật khẩu thất bại, hãy thử lại!',
    });
  }
};

export const contact = async (req, res) => {
  const { email, fullName, content, phone } = req.body;
  if (!email || !fullName || !content)
    return res
      .status(400)
      .json({ success: false, message: 'Vui lòng điền đầy đủ thông tin!' });

  try {
    const adminData = await UserModel.find({
      role: RoleUserEnum.ADMIN,
    });

    // mailer
    axios.post(`${apiNotificationUrl}/auth/contact`, {
      content,
      phone,
      fullName,
      email,
      adminData,
    });

    res
      .status(200)
      .json({ success: true, message: 'Gửi email liên hệ thành công' });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: 'Gửi email thất bại, hãy thử lại.' });
  }
};

export const loginGoogle = async (req, res) => {
  try {
    let email;
    let name;
    let picture;
    if (req.body?.credential) {
      const client = new OAuth2Client(process.env.CLIENT_ID_GOOGLE);
      const ticket = await client.verifyIdToken({
        idToken: req.body.credential,
        audience: process.env.CLIENT_ID_GOOGLE,
      });
      const decoded = ticket.getPayload();
      if (!decoded?.email || !decoded?.name)
        return res
          .status(400)
          .json({ success: false, message: 'Token không hợp lệ' });
      email = decoded.email;
      name = decoded.name;
      picture = decoded.picture;
    } else {
      email = req.body?.email;
      name = req.body?.name;
      picture = req.body?.photo;
    }
    const userExisted = await UserModel.findOne({ email });
    let user;
    if (!userExisted) {
      const eager_options = {
        width: 770,
        height: 627,
        crop: 'fill',
        format: 'png',
      };
      const result = await cloudinary.uploader.upload(picture, eager_options);
      const password = await argon2.hash('123123');
      user = new UserModel({
        username: email,
        email,
        fullName: name,
        password,
        avatar: { imageUrl: result.url, public_id: result.public_id },
      });
      await user.save();

      // mailer
      axios.post(`${apiNotificationUrl}/auth/activate`, {
        userId: user._id,
        email,
      });
      // chat
      migrateUserToChat(user);
    } else {
      user = userExisted;
    }

    const accessToken = jwt.sign(
      { userId: user._id },
      process.env.ACCESS_TOKEN_SECRET,
      { expiresIn: '7d' }
    );

    res.status(200).json({ success: true, accessToken });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: 'Đăng ký thất bại' });
  }
};

const migrateUserToChat = (newUser) => {
  try {
    // chat
    axios.post(`${apiChatUrl}/auth/migrate/user`, newUser, {
      headers: {
        Authorization: `Bearer ${process.env.ADMIN_CHAT_TOKEN}`,
      },
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: 'Internal server error' });
  }
};
