import https from 'https';

export const createOrderGHN = ({
  note,
  to_name,
  to_phone,
  to_address,
  to_ward_code,
  to_district_id,
  cod_amount,
  items,
}) =>
  new Promise((resolve, reject) => {
    try {
      const requestBody = JSON.stringify({
        payment_type_id: 2,
        note,
        required_note: 'CHOXEMHANGKHONGTHU',
        to_name,
        to_phone,
        to_address,
        to_ward_code,
        to_district_id,
        cod_amount,
        insurance_value: 4000000,
        weight: 1,
        length: 1,
        width: 1,
        height: 1,
        service_id: 0,
        service_type_id: 2,
        items,
      });
      const options = {
        hostname: 'dev-online-gateway.ghn.vn',
        path: '/shiip/public-api/v2/shipping-order/create',
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Content-Length': Buffer.byteLength(requestBody),
          Token: `${process.env.GHN_TOKEN_API}`,
        },
      };
      const request = https.request(options, (response) => {
        response.setEncoding('utf8');
        response.on('data', async (body) => {
          const bodyParse = JSON.parse(body);
          resolve(bodyParse?.data?.order_code);
        });
      });
      request.on('error', (e) => {
        console.log(`problem with GHN request: ${e.message}`);
      });
      request.write(requestBody);
      request.end();
    } catch (error) {
      console.log(error);
      reject(error);
    }
  });

export const cancelOrderGHN = (order_codes) =>
  new Promise((resolve, reject) => {
    try {
      const requestBody = JSON.stringify({
        order_codes,
      });
      const options = {
        hostname: 'dev-online-gateway.ghn.vn',
        path: '/shiip/public-api/v2/switch-status/cancel',
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Content-Length': Buffer.byteLength(requestBody),
          Token: `${process.env.GHN_TOKEN_API}`,
          ShopId: `${process.env.GHN_SHOP_ID}`,
        },
      };
      const request = https.request(options, (response) => {
        response.setEncoding('utf8');
        response.on('data', async (body) => {
          const bodyParse = JSON.parse(body);
          resolve(bodyParse?.data?.result);
        });
      });
      request.on('error', (e) => {
        console.log(`problem with GHN request: ${e.message}`);
      });
      request.write(requestBody);
      request.end();
    } catch (error) {
      console.log(error);
      reject(error);
    }
  });
