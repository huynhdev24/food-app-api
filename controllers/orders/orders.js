import { OrderDeliveryMethodEnum } from '@nhuthuynh.phu/yum-yum-common/dist/order/enums.js';
import { OrderModel } from '@nhuthuynh.phu/yum-yum-common/dist/order/model.js';
import { ProductModel } from '@nhuthuynh.phu/yum-yum-common/dist/product/model.js';
import { UserModel } from '@nhuthuynh.phu/yum-yum-common/dist/user/model.js';
import axios from 'axios';
import jwt from 'jsonwebtoken';
import { apiNotificationUrl } from '../../constants/index.js';
import { redisClient } from '../../index.js';
import { clearCache } from '../../utils/clearCache.js';
import { cancelOrderGHN, createOrderGHN } from './ghn.orders.js';
// import { WarehouseModel } from '@nhuthuynh.phu/yum-yum-common/dist/warehouse/model.js';
// import nearestPoint from '@turf/nearest-point';
// import { getDistance } from 'geolib';
// import point from 'turf-point';
// import { getLatlngFromAddr } from '../map.js';

export const getAllOrders = async (req, res) => {
  try {
    let orders;
    const { isDelivery, deliveryMethod, isPayment } = req.query;
    if (isDelivery && deliveryMethod && isPayment) {
      orders = await OrderModel.find({
        isDelivery,
        deliveryMethod,
        isPayment: { $ne: isPayment },
      })
        .sort({ createdAt: -1 })
        .populate([
          {
            path: 'userId',
            select: ['id', 'fullName', 'address', 'phone'],
          },
          {
            path: 'productIds.productId',
            select: ['id', 'name', 'categoryId', 'images'],
            populate: {
              path: 'categoryId',
              select: ['name'],
            },
          },
        ]);
    } else {
      orders = await OrderModel.find()
        .sort({ createdAt: -1 })
        .populate([
          {
            path: 'userId',
            select: ['id', 'fullName', 'address', 'phone'],
          },
          {
            path: 'productIds.productId',
            select: ['id', 'name', 'categoryId', 'images'],
            populate: {
              path: 'categoryId',
              select: ['name'],
            },
          },
        ]);
    }
    const result = { success: true, orders };
    await redisClient.set(req.originalUrl, JSON.stringify(result), {
      EX: 60 * 60 * 24 * 7,
    });
    res.status(200).json(result);
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

export const getUserOrders = async (req, res) => {
  try {
    const orders = await OrderModel.find({
      userId: req.userId,
    })
      .sort({ createdAt: -1 })
      .populate([
        {
          path: 'userId',
          select: ['id', 'fullName', 'address', 'phone'],
        },
        {
          path: 'productIds.productId',
          select: ['id', 'name'],
        },
      ]);
    res.status(200).json({ success: true, orders });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

export const getUserOrder = async (req, res) => {
  try {
    const order = await OrderModel.findOne({
      userId: req.userId,
      _id: req.params.id + '',
    }).populate([
      {
        path: 'userId',
        select: ['id', 'fullName', 'address', 'phone'],
      },
      {
        path: 'productIds.productId',
        select: ['id', 'name'],
      },
    ]);
    res.status(200).json({ success: true, order });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

// export const getOrderRoute = async (req, res) => {
//   try {
//     const [order, warehouses] = await Promise.all([
//       OrderModel.findOne({ _id: req.params.id }).populate([
//         {
//           path: 'userId',
//           select: ['id', 'fullName', 'address', 'phone', 'latlng'],
//         },
//         {
//           path: 'productIds.productId',
//           select: ['id', 'name'],
//         },
//       ]),
//       WarehouseModel.find().sort({ createdAt: -1 }),
//     ]);
//     if (!order?.userId?.latlng && !order?.userId?.address) {
//       return res.status(400).json({
//         success: false,
//         message: 'Vui lòng cập nhật thông tin đầy đủ',
//       });
//     }

//     let start;
//     let minIndex = 0;
//     let min = 999999;
//     if (order?.userId?.latlng) {
//       start = point([order?.userId?.latlng.lng, order?.userId?.latlng.lat]);
//       await Promise.all(
//         warehouses.map(async (warehouse, index) => {
//           const warehouseCoor = await getLatlngFromAddr(warehouse.address);
//           if (warehouseCoor?.length > 0) {
//             const distance = getDistance(
//               {
//                 latitude: order?.userId?.latlng.lat,
//                 longitude: order?.userId?.latlng.lng,
//               },
//               {
//                 latitude: warehouseCoor[0]?.latitude,
//                 longitude: warehouseCoor[0]?.longitude,
//               }
//             );
//             if (distance < min) {
//               min = distance;
//               minIndex = index;
//             }
//           }
//         })
//       );
//     } else {
//       const coor = await getLatlngFromAddr(order?.userId?.address);
//       start = point([coor[0]?.longitude, coor[0]?.latitude]);
//       await Promise.all(
//         warehouses.map(async (warehouse, index) => {
//           const warehouseCoor = await getLatlngFromAddr(warehouse.address);
//           if (warehouseCoor?.length > 0) {
//             const distance = getDistance(
//               { latitude: coor[0]?.latitude, longitude: coor[0]?.longitude },
//               {
//                 latitude: warehouseCoor[0]?.latitude,
//                 longitude: warehouseCoor[0]?.longitude,
//               }
//             );
//             if (distance < min) {
//               min = distance;
//               minIndex = index;
//             }
//           }
//         })
//       );
//     }

//     const startPoint = nearestPoint(start, geojsonPoint);
//     const warehouseCoor = await getLatlngFromAddr(warehouses[minIndex].address);
//     const finish = point([
//       warehouseCoor[0]?.longitude,
//       warehouseCoor[0]?.latitude,
//     ]);
//     const finishPoint = nearestPoint(finish, geojsonPoint);
//     const path = pathFinder.findPath(finishPoint, startPoint);
//     res.status(200).json({
//       success: true,
//       route: {
//         ...path,
//         path: [
//           [warehouseCoor[0]?.longitude, warehouseCoor[0]?.latitude],
//           ...path?.path,
//         ],
//       },
//     });
//   } catch (error) {
//     console.log(error);
//     res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
//   }
// };

export const createOrder = async (req, res) => {
  const { orderDetail, deliveryFee, note, deliveryMethod } = req.body;
  if (orderDetail.length === 0 || !deliveryMethod)
    return res.status(400).json({
      success: false,
      message: 'Vui lòng điền đầy đủ thông tin',
    });

  try {
    const productIds = [];
    const items = [];
    let totalPrice = 0;
    const user = await UserModel.findOne({ _id: req.userId });
    await Promise.all(
      orderDetail.map(async (orderItem) => {
        const product = await ProductModel.findOneAndUpdate(
          { _id: orderItem.id },
          { $inc: { quantity: -orderItem.quantity } },
          { new: true }
        );
        if (!product) {
          return res.status(404).json({
            success: false,
            message: `Không tìm thấy sản phẩm ${orderItem.id}`,
          });
        }
        const price = (product.price - product.discount) * orderItem.quantity;
        productIds.push({
          productId: product._id,
          quantity: orderItem.quantity,
          color: orderItem.color,
          size: orderItem.size,
          price,
        });
        items.push({
          name: product.name,
          quantity: orderItem.quantity,
          price,
        });
        totalPrice += price;
      })
    );

    let orderCodeGHN = '';
    if (deliveryMethod === OrderDeliveryMethodEnum.GHN) {
      orderCodeGHN = await createOrderGHN({
        note,
        to_name: user.fullName,
        to_phone: user.phone,
        to_address: user.address,
        to_ward_code: user.wardCode,
        to_district_id: user.districtId,
        cod_amount: totalPrice,
        items,
      });
    }
    let order = new OrderModel({
      productIds,
      totalPrice: totalPrice + deliveryFee,
      userId: req.userId,
      note: note ?? '',
      deliveryFee,
      orderCodeGHN,
      deliveryMethod,
    });
    await order.save();
    order = await order.populate([
      {
        path: 'userId',
        select: ['id', 'fullName', 'address', 'phone', 'email'],
      },
      {
        path: 'productIds.productId',
        select: ['id', 'name'],
      },
    ]);

    // mailer
    axios.post(`${apiNotificationUrl}/orders/create-order`, {
      fullName: order.userId.fullName,
      address: order.userId.address,
      phone: order.userId.phone,
      orderId: order._id,
      totalPrice: order.totalPrice,
      payment: 'Thanh toán khi nhận hàng',
      note: order.note,
      productIds: order.productIds,
      isCancel: false,
      email: order.userId.email,
      userId: order.userId._id,
    });
    await clearCache('/orders');
    await clearCache('/products');

    res
      .status(200)
      .json({ success: true, message: 'Tạo đơn hàng thành công', order });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

export const updateOrder = async (req, res) => {
  const { isPayment, isDelivery } = req.body;
  if (isPayment === undefined || isDelivery === undefined)
    return res.status(400).json({
      success: false,
      message: 'Vui lòng điền đầy đủ thông tin',
    });

  try {
    const orderExisted = await OrderModel.findOne({ _id: req.params.id });
    if (!orderExisted) {
      return res.status(404).json({
        success: false,
        message: 'Không tìm thấy đơn hàng',
      });
    }
    // if (orderExisted.isDelivery === 2 || orderExisted.isDelivery === 3) {
    //   return res.status(400).json({
    //     success: false,
    //     message: 'Đơn hàng đã giao/hủy không thể thay đổi',
    //   });
    // }

    const tasks = [];
    tasks.push(
      OrderModel.findOneAndUpdate(
        { _id: req.params.id },
        {
          isDelivery,
          isPayment,
        },
        { new: true }
      ).populate([
        {
          path: 'userId',
          select: ['id', 'fullName', 'address', 'phone'],
        },
        {
          path: 'productIds.productId',
          select: ['id', 'name'],
        },
      ])
    );

    if (isDelivery === 2) {
      const coin = Math.floor(orderExisted.totalPrice / 1000);
      tasks.push(
        UserModel.findOneAndUpdate(
          { _id: req.userId },
          {
            $inc: { coin },
          },
          { new: true }
        )
      );

      // mailer
      axios.post(`${apiNotificationUrl}/orders/update-order`, {
        userId: orderExisted.userId,
        orderId: orderExisted._id,
        coin,
      });
    }
    tasks.push(clearCache('/orders'));
    const [order] = await Promise.all(tasks);

    res
      .status(200)
      .json({ success: true, message: 'Cập nhật đơn hàng thành công', order });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

export const updateCheckout = async (req, res) => {
  const { token, id } = req.body;
  if (!token || !id)
    return res
      .status(400)
      .json({ success: false, message: 'Thiếu token hoặc ID' });

  try {
    const decoded = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);
    if (decoded.orderId !== id)
      return res
        .status(400)
        .json({ success: false, message: 'Token hoặc ID không hợp lệ' });

    const order = await OrderModel.findOneAndUpdate(
      { _id: id },
      { isPayment: 1 },
      { new: true }
    ).populate([
      {
        path: 'userId',
        select: ['id', 'fullName', 'address', 'phone', 'email'],
      },
      {
        path: 'productIds.productId',
        select: ['id', 'name'],
      },
    ]);

    // mailer
    axios.post(`${apiNotificationUrl}/orders/update-checkout`, {
      fullName: order.userId.fullName,
      address: order.userId.address,
      phone: order.userId.phone,
      orderId: order._id,
      totalPrice: order.totalPrice,
      payment: 'Đã thanh toán',
      note: order.note,
      productIds: order.productIds,
      isCancel: false,
      email: order.userId.email,
      userId: order.userId._id,
    });
    await clearCache('/orders');
    res
      .status(200)
      .json({ success: true, message: 'Bạn đã thanh toán thành công!' });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: 'Token không hợp lệ hoặc đã hết hạn' });
  }
};

export const cancelOrder = async (req, res) => {
  const { note } = req.body;
  if (!note)
    return res
      .status(400)
      .json({ success: false, message: 'Vui lòng điền đầy đủ thông tin' });

  try {
    const order = await OrderModel.findOneAndUpdate(
      { _id: req.params.id },
      { isDelivery: 3, note: `Lí do hủy đơn: ${note}` },
      { new: true }
    ).populate([
      {
        path: 'userId',
        select: ['id', 'fullName', 'address', 'phone', 'email'],
      },
      {
        path: 'productIds.productId',
        select: ['id', 'name'],
      },
    ]);
    if (!order)
      return res
        .status(404)
        .json({ success: false, message: 'Không tìm thấy đơn hàng' });
    await Promise.all([
      order.productIds.map(async (orderItem) => {
        await ProductModel.findOneAndUpdate(
          { _id: orderItem.productId },
          { $inc: { quantity: orderItem.quantity } },
          { new: true }
        );
      }),
      clearCache('/orders'),
      clearCache('/products'),
    ]);
    await cancelOrderGHN([`${order.orderCodeGHN}`]);

    // mailer
    axios.post(`${apiNotificationUrl}/orders/cancel-order`, {
      fullName: order.userId.fullName,
      address: order.userId.address,
      phone: order.userId.phone,
      orderId: order._id,
      totalPrice: order.totalPrice,
      payment: 'Chưa thanh tóan',
      note: order.note,
      productIds: order.productIds,
      isCancel: true,
      email: order.userId.email,
      userId: order.userId._id,
    });

    res.status(200).json({
      success: true,
      message: 'Bạn đã hủy đơn thành công! Vui lòng load lại trang',
      order,
    });
  } catch (error) {
    console.log(error);
    return res
      .status(500)
      .json({ success: false, message: 'Token không hợp lệ hoặc đã hết hạn' });
  }
};
