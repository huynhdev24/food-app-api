import { OrderDeliveryMethodEnum } from '@nhuthuynh.phu/yum-yum-common/dist/order/enums.js';
import { OrderModel } from '@nhuthuynh.phu/yum-yum-common/dist/order/model.js';
import { ProductModel } from '@nhuthuynh.phu/yum-yum-common/dist/product/model.js';
import { UserModel } from '@nhuthuynh.phu/yum-yum-common/dist/user/model.js';
import crypto from 'crypto';
import https from 'https';
import jwt from 'jsonwebtoken';
import { clearCache } from '../../utils/clearCache.js';
import { createOrderGHN } from './ghn.orders.js';

export const momoPayment = async (req, res) => {
  const { orderDetail, deliveryFee, deliveryMethod } = req.body;
  if (!orderDetail || !deliveryMethod)
    return res.status(400).json({
      success: false,
      message: 'Vui lòng điền đầy đủ thông tin',
    });

  try {
    const productIds = [];
    const ghn_items = [];
    let totalPrice = 0;
    const [authUser] = await Promise.all([
      UserModel.findById(req.userId),
      Promise.all(
        orderDetail.map(async (orderItem) => {
          const product = await ProductModel.findOneAndUpdate(
            { _id: orderItem.id },
            { $inc: { quantity: -orderItem.quantity } },
            { new: true }
          );
          if (!product) {
            return res.status(404).json({
              success: false,
              message: `Không tìm thấy sản phẩm ${orderItem.id}`,
            });
          }
          const price = (product.price - product.discount) * orderItem.quantity;
          productIds.push({
            productId: product._id,
            quantity: orderItem.quantity,
            color: orderItem.color,
            size: orderItem.size,
            price,
          });
          ghn_items.push({
            name: product.name,
            quantity: orderItem.quantity,
            price: price,
          });
          totalPrice += price;
        })
      ),
    ]);

    let orderCodeGHN = '';
    if (deliveryMethod === OrderDeliveryMethodEnum.GHN) {
      orderCodeGHN = await createOrderGHN({
        note: req?.body?.note ?? '',
        to_name: authUser.fullName,
        to_phone: authUser.phone,
        to_address: authUser.address,
        to_ward_code: authUser.wardCode,
        to_district_id: authUser.districtId,
        cod_amount: 0,
        items: ghn_items,
      });
    }
    let order = new OrderModel({
      productIds,
      totalPrice: totalPrice + parseFloat(deliveryFee),
      userId: req.userId,
      note: req?.body?.note ?? '',
      deliveryFee,
      orderIdMomo: 123456,
      orderCodeGHN,
      deliveryMethod,
    });
    await order.save();

    const token = jwt.sign(
      { orderId: order._id },
      process.env.ACCESS_TOKEN_SECRET,
      { expiresIn: '10m' }
    );

    var partnerCode = process.env.MOMO_PARTNER_CODE;
    var accessKey = process.env.MOMO_ACCESS_KEY;
    var secretKey = process.env.MOMO_SECRET_KEY;
    var dateTimestamp = new Date().getTime();
    var requestId = partnerCode + dateTimestamp;
    var orderId = requestId;
    var orderInfo = 'Thanh toán MoMo';
    var redirectUrl = `${process.env.CLIENT_URL}/payment/success?order_id=${order._id}&token=${token}`;
    var ipnUrl = 'https://callback.url/notify';
    // var ipnUrl = redirectUrl = "https://webhook.site/454e7b77-f177-4ece-8236-ddf1c26ba7f8";
    var amount = totalPrice.toString();
    var requestType = 'captureWallet';
    var extraData = ''; //pass empty value if your merchant does not have stores

    //before sign HMAC SHA256 with format
    //accessKey=$accessKey&amount=$amount&extraData=$extraData&ipnUrl=$ipnUrl&orderId=$orderId&orderInfo=$orderInfo&partnerCode=$partnerCode&redirectUrl=$redirectUrl&requestId=$requestId&requestType=$requestType
    var rawSignature =
      'accessKey=' +
      accessKey +
      '&amount=' +
      amount +
      '&extraData=' +
      extraData +
      '&ipnUrl=' +
      ipnUrl +
      '&orderId=' +
      orderId +
      '&orderInfo=' +
      orderInfo +
      '&partnerCode=' +
      partnerCode +
      '&redirectUrl=' +
      redirectUrl +
      '&requestId=' +
      requestId +
      '&requestType=' +
      requestType;

    //signature
    var signature = crypto
      .createHmac('sha256', secretKey)
      .update(rawSignature)
      .digest('hex');

    //json object send to MoMo endpoint
    const requestBody = JSON.stringify({
      partnerCode: partnerCode,
      accessKey: accessKey,
      requestId: requestId,
      amount: amount,
      orderId: orderId,
      orderInfo: orderInfo,
      redirectUrl: redirectUrl,
      ipnUrl: ipnUrl,
      extraData: extraData,
      requestType: requestType,
      signature: signature,
      lang: 'vi',
    });
    //Create the HTTPS objects
    const options = {
      hostname: 'test-payment.momo.vn',
      port: 443,
      path: '/v2/gateway/api/create',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Content-Length': Buffer.byteLength(requestBody),
      },
    };
    //Send the request and get the response
    const request = https.request(options, (response) => {
      response.setEncoding('utf8');
      response.on('data', async (body) => {
        const bodyParse = JSON.parse(body);
        if (bodyParse.resultCode === 0) {
          await OrderModel.findOneAndUpdate(
            { _id: order._id },
            { orderIdMomo: orderId },
            { new: true }
          );
          return res.status(200).json({
            success: true,
            message: 'Tạo đơn hàng thành công',
            payUrl: bodyParse.payUrl,
          });
        } else {
          console.log(bodyParse);
          return res.status(400).json({
            success: false,
            message: bodyParse.message,
          });
        }
      });
    });

    request.on('error', (e) => {
      console.log(`problem with MoMo request: ${e.message}`);
    });
    await clearCache('/orders');
    await clearCache('/products');

    // write data to request body
    request.write(requestBody);
    request.end();
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};
