import { OrderDeliveryMethodEnum } from '@nhuthuynh.phu/yum-yum-common/dist/order/enums.js';
import { OrderModel } from '@nhuthuynh.phu/yum-yum-common/dist/order/model.js';
import { ProductModel } from '@nhuthuynh.phu/yum-yum-common/dist/product/model.js';
import { UserModel } from '@nhuthuynh.phu/yum-yum-common/dist/user/model.js';
import axios from 'axios';
import jwt from 'jsonwebtoken';
import Stripe from 'stripe';
import { apiNotificationUrl } from '../../constants/index.js';
import { clearCache } from '../../utils/clearCache.js';
import { cancelOrderGHN, createOrderGHN } from './ghn.orders.js';

export const createCheckout = async (req, res) => {
  const { orderDetail, deliveryFee, isUseCoin, deliveryMethod } = req.body;
  if (!orderDetail || !deliveryMethod)
    return res.status(400).json({
      success: false,
      message: 'Vui lòng điền đầy đủ thông tin',
    });

  try {
    const productIds = [];
    const line_items = [];
    const ghn_items = [];
    let totalPrice = 0;
    const authUser = await UserModel.findById(req.userId);
    let coinExist = isUseCoin ? authUser?.coin || 0 : 0;
    await Promise.all(
      orderDetail.map(async (orderItem) => {
        const product = await ProductModel.findOneAndUpdate(
          { _id: orderItem.id },
          { $inc: { quantity: -orderItem.quantity } },
          { new: true }
        );
        if (!product) {
          return res.status(404).json({
            success: false,
            message: `Không tìm thấy sản phẩm ${orderItem.id}`,
          });
        }
        const price = (product.price - product.discount) * orderItem.quantity;
        let unit_amount = product.price - product.discount;
        if (coinExist > 0) {
          unit_amount =
            product.price -
            product.discount -
            Math.round(coinExist / orderItem.quantity);
          if (unit_amount < 0) {
            coinExist = -unit_amount;
            unit_amount = 0;
          }
        }
        productIds.push({
          productId: product._id,
          quantity: orderItem.quantity,
          color: orderItem.color,
          size: orderItem.size,
          price,
        });
        line_items.push({
          price_data: {
            currency: 'vnd',
            product_data: {
              name: product.name,
              images: product.images.map((image) => image.imageUrl),
            },
            unit_amount,
          },
          quantity: orderItem.quantity,
        });
        ghn_items.push({
          name: product.name,
          quantity: orderItem.quantity,
          price: price,
        });
        totalPrice += price;
      })
    );

    const totalPriceWithDeliveryFee = totalPrice + parseFloat(deliveryFee);
    let orderCodeGHN = '';
    if (deliveryMethod === OrderDeliveryMethodEnum.GHN) {
      orderCodeGHN = await createOrderGHN({
        note: req?.body?.note ?? '',
        to_name: authUser.fullName,
        to_phone: authUser.phone,
        to_address: authUser.address,
        to_ward_code: authUser.wardCode,
        to_district_id: authUser.districtId,
        cod_amount: 0,
        items: ghn_items,
      });
    }
    let order = new OrderModel({
      productIds,
      totalPrice:
        totalPriceWithDeliveryFee -
        (isUseCoin
          ? authUser?.coin <= totalPriceWithDeliveryFee
            ? authUser?.coin
            : totalPriceWithDeliveryFee
          : 0),
      userId: req.userId,
      note: req?.body?.note ?? '',
      deliveryFee,
      orderCodeGHN,
      deliveryMethod,
    });
    await order.save();

    const token = jwt.sign(
      { orderId: order._id },
      process.env.ACCESS_TOKEN_SECRET,
      { expiresIn: '200m' }
    );
    const stripe = Stripe(process.env.STRIPE_SECRET_KEY);

    const session = await stripe.checkout.sessions.create({
      shipping_options: [
        {
          shipping_rate_data: {
            type: 'fixed_amount',
            display_name:
              deliveryMethod === OrderDeliveryMethodEnum.GHN ? 'GHN' : 'YumYum',
            fixed_amount: {
              amount: deliveryFee,
              currency: 'vnd',
            },
          },
        },
      ],
      payment_method_types: ['card'],
      line_items,
      mode: 'payment',
      success_url: `${process.env.CLIENT_URL}/payment/success?order_id=${order._id}&token=${token}`,
      cancel_url: `${process.env.CLIENT_URL}/payment/failed`,
    });

    const updateTasks = [];
    updateTasks.push(
      OrderModel.findOneAndUpdate(
        { _id: order._id },
        { paymentIntentStripe: session.payment_intent },
        { new: true }
      )
    );
    if (isUseCoin) {
      updateTasks.push(
        UserModel.findOneAndUpdate(
          { _id: req.userId },
          {
            coin:
              authUser?.coin <= totalPriceWithDeliveryFee
                ? 0
                : authUser?.coin - totalPriceWithDeliveryFee,
          },
          { new: true }
        )
      );
    }
    updateTasks.push(clearCache('/orders'));
    updateTasks.push(clearCache('/products'));
    await Promise.all(updateTasks);

    res.status(200).json({
      success: true,
      message: 'Tạo đơn hàng thành công',
      sessionId: session.id,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

export const refundStripe = async (req, res) => {
  const { payment_intent, note } = req.body;
  if (!payment_intent || !note)
    return res
      .status(400)
      .json({ success: false, message: 'Vui lòng điền đầy đủ thông tin' });

  try {
    const stripe = Stripe(process.env.STRIPE_SECRET_KEY);
    const refund = await stripe.refunds.create({ payment_intent });
    if (refund.status === 'succeeded') {
      const order = await OrderModel.findOneAndUpdate(
        { _id: req.params.id },
        { isDelivery: 3, isPayment: 2, note: `Lí do hủy đơn: ${note}` },
        { new: true }
      ).populate([
        {
          path: 'userId',
          select: ['id', 'fullName', 'address', 'phone', 'email'],
        },
        {
          path: 'productIds.productId',
          select: ['id', 'name'],
        },
      ]);
      if (!order)
        return res
          .status(404)
          .json({ success: false, message: 'Không tìm thấy đơn hàng' });
      await Promise.all([
        order.productIds.map(async (orderItem) => {
          await ProductModel.findOneAndUpdate(
            { _id: orderItem.productId },
            { $inc: { quantity: orderItem.quantity } },
            { new: true }
          );
        }),
        clearCache('/orders'),
        clearCache('/products'),
      ]);
      await cancelOrderGHN([`${order.orderCodeGHN}`]);

      // mailer
      axios.post(`${apiNotificationUrl}/orders/cancel-order`, {
        fullName: order.userId.fullName,
        address: order.userId.address,
        phone: order.userId.phone,
        orderId: order._id,
        totalPrice: order.totalPrice,
        payment: 'Đã hoàn tiền',
        note: order.note,
        productIds: order.productIds,
        isCancel: true,
        email: order.userId.email,
        userId: order.userId._id,
      });

      res
        .status(200)
        .json({ success: true, message: 'Bạn đã hủy đơn thành công!', order });
    } else {
      console.log(refund);
      res
        .status(400)
        .json({ success: false, message: 'Bạn đã hủy đơn thất bại!' });
    }
  } catch (error) {
    console.log(error);
    return res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};
