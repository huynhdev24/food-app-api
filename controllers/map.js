import NodeGeocoder from 'node-geocoder';

export const getLatlng = async (req, res) => {
  try {
    const { address } = req.body;
    if (!address)
      return res.status(400).json({ success: false, message: 'Empty address' });
    const result = await getLatlngFromAddr(address);
    res.status(200).json(result[0]);
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

export const getLatlngFromAddr = async (address) => {
  const options = {
    provider: 'opencage',

    // Optional depending on the providers
    // fetch: customFetchImplementation,
    apiKey: process.env.OPENCAGE_API_KEY, // for Mapquest, OpenCage, Google Premier
    formatter: null, // 'gpx', 'string', ...
  };

  const geocoder = NodeGeocoder(options);

  // Using callback
  return geocoder.geocode(address);
};
