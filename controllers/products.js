import { CommentModel } from '@nhuthuynh.phu/yum-yum-common/dist/comment/model.js';
import { OrderModel } from '@nhuthuynh.phu/yum-yum-common/dist/order/model.js';
import { ProductModel } from '@nhuthuynh.phu/yum-yum-common/dist/product/model.js';
import { v2 as cloudinary } from 'cloudinary';
import fs from 'fs';
import { redisClient } from '../index.js';
import { clearCache } from '../utils/clearCache.js';

export const getProducts = async (req, res) => {
  try {
    const products = await ProductModel.find().populate([
      {
        path: 'categoryId',
        select: ['id', 'name'],
      },
    ]);
    const result = { success: true, products };
    await redisClient.set(req.originalUrl, JSON.stringify(result), {
      EX: 60 * 60 * 24 * 7,
    });
    res.status(200).json(result);
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

export const createProduct = async (req, res) => {
  const { name, unit, categoryId } = req.body;
  if (!name || !unit || !categoryId)
    return res.status(400).json({
      success: false,
      message: 'Vui lòng điền đầy đủ thông tin',
    });

  try {
    const productExisted = await ProductModel.findOne({ name });
    if (productExisted) {
      return res
        .status(400)
        .json({ success: false, message: 'Trùng tên sản phẩm!' });
    }

    const newProduct = req.body;
    let product = new ProductModel({
      ...newProduct,
      seoUrl: name
        .replace(/\s/g, '-')
        .toLowerCase()
        .normalize('NFD')
        .replace(/[\u0300-\u036f]/g, ''),
    });
    await product.save();
    product = await product.populate([
      {
        path: 'categoryId',
        select: ['id', 'name'],
      },
    ]);
    await clearCache('/products');
    res.status(200).json({
      success: true,
      message: 'Tạo sản phẩm mới thành công!',
      product,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

export const updateProduct = async (req, res) => {
  const { name, unit, categoryId, review } = req.body;
  if ((!name || !unit || !categoryId) && !review)
    return res.status(400).json({
      success: false,
      message: 'Vui lòng điền đầy đủ thông tin',
    });

  try {
    const updateProduct = req.body;
    const product = await ProductModel.findOneAndUpdate(
      { _id: req.params.id },
      {
        ...updateProduct,
        seoUrl: name
          ?.replace(/\s/g, '-')
          ?.toLowerCase()
          ?.normalize('NFD')
          ?.replace(/[\u0300-\u036f]/g, ''),
      },
      { new: true, omitUndefined: true }
    ).populate([
      {
        path: 'categoryId',
        select: ['id', 'name'],
      },
    ]);
    if (!product)
      return res
        .status(404)
        .json({ success: false, message: 'Không tìm thấy sản phẩm' });
    await clearCache('/products');
    res.status(200).json({
      success: true,
      message: 'Cập nhật sản phẩm thành công!',
      product,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

export const deleteProduct = async (req, res) => {
  try {
    const order = await OrderModel.findOne({
      'productIds.productId': req.params.id,
    });
    if (order) {
      return res
        .status(400)
        .json({ success: false, message: 'Không thể xoá!' });
    }

    const product = await ProductModel.findOneAndDelete({
      _id: req.params.id,
    });
    if (!product)
      return res
        .status(404)
        .json({ success: false, message: 'Không tìm thấy sản phẩm' });
    const tasks = [];
    product?.images?.forEach(async (item) => {
      tasks.push(cloudinary.uploader.destroy(item.public_id));
    });
    tasks.push(CommentModel.deleteMany({ productId: req.params.id }));
    tasks.push(clearCache('/products'));
    await Promise.all(tasks);

    res
      .status(200)
      .json({ success: true, message: 'Xoá thành công!', product });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

export const addImage = async (req, res) => {
  try {
    const eager_options = {
      width: 770,
      height: 627,
      crop: 'fill',
      format: 'png',
    };
    const result = await cloudinary.uploader.upload(
      req.file.path,
      eager_options
    );
    fs.unlinkSync(req.file.path);
    const product = await ProductModel.findOneAndUpdate(
      { _id: req.params.id },
      {
        $addToSet: {
          images: { imageUrl: result.url, public_id: result.public_id },
        },
      },
      { new: true }
    ).populate([
      {
        path: 'categoryId',
        select: ['id', 'name'],
      },
    ]);
    await clearCache('/products');
    res.status(200).json({
      success: true,
      message: 'Cập nhật sản phẩm thành công!',
      product,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

export const deleteImage = async (req, res) => {
  try {
    const publicId = req.params.publicId;
    const product = await ProductModel.findOneAndUpdate(
      { _id: req.params.id },
      {
        $pull: {
          images: { public_id: { $in: [publicId] } },
        },
      },
      { new: true }
    ).populate([
      {
        path: 'categoryId',
        select: ['id', 'name'],
      },
    ]);
    await Promise.all([
      cloudinary.uploader.destroy(publicId),
      clearCache('/products'),
    ]);
    res.status(200).json({
      success: true,
      message: 'Cập nhật sản phẩm thành công!',
      product,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

export const getLatestProducts = async (req, res) => {
  try {
    const products = await ProductModel.find().limit(5).sort({ createdAt: -1 });
    const result = { success: true, products };
    await redisClient.set(req.originalUrl, JSON.stringify(result), {
      EX: 60 * 60 * 24 * 7,
    });
    res.status(200).json(result);
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

export const getTopRatingProducts = async (req, res) => {
  try {
    const products = await ProductModel.find().limit(5).sort({ rating: -1 });
    const result = { success: true, products };
    await redisClient.set(req.originalUrl, JSON.stringify(result), {
      EX: 60 * 60 * 24 * 7,
    });
    res.status(200).json(result);
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

export const getTopDiscountProducts = async (req, res) => {
  try {
    const products = await ProductModel.find().limit(6).sort({ discount: -1 });
    const result = { success: true, products };
    await redisClient.set(req.originalUrl, JSON.stringify(result), {
      EX: 60 * 60 * 24 * 7,
    });
    res.status(200).json(result);
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

export const getRelatedProducts = async (req, res) => {
  try {
    const products = await ProductModel.find({
      categoryId: req.params.id,
    }).limit(4);
    const result = { success: true, products };
    await redisClient.set(req.originalUrl, JSON.stringify(result), {
      EX: 60 * 60 * 24 * 7,
    });
    res.status(200).json(result);
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

export const getProductDetail = async (req, res) => {
  try {
    const idParam = req.params.id;
    const product = idParam.includes('-')
      ? await ProductModel.findOne({
          seoUrl: idParam,
        }).populate([
          {
            path: 'categoryId',
            select: ['id', 'name'],
          },
        ])
      : await ProductModel.findOne({
          _id: idParam,
        }).populate([
          {
            path: 'categoryId',
            select: ['id', 'name'],
          },
        ]);
    if (!product)
      return res
        .status(404)
        .json({ success: false, message: 'Không tìm thấy sản phẩm' });

    const result = { success: true, product };
    await redisClient.set(req.originalUrl, JSON.stringify(result), {
      EX: 60 * 60 * 24 * 7,
    });
    res.status(200).json(result);
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

// export const migrateSeoUrlProducts = async (_, res) => {
//   try {
//     const products = await ProductModel.find({}, { name: 1 });
//     const tasks = [];
//     products.forEach((product) =>
//       tasks.push(
//         ProductModel.findOneAndUpdate(
//           { _id: product._id },
//           {
//             seoUrl: product.name
//               .replace(/\s/g, '-')
//               .toLowerCase()
//               .normalize('NFD')
//               .replace(/[\u0300-\u036f]/g, ''),
//           },
//           { new: true }
//         )
//       )
//     );
//     await Promise.all(tasks);
//     res.status(200).json({ success: true, products });
//   } catch (error) {
//     console.log(error);
//     res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
//   }
// };
