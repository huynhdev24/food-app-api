import { CategoryModel } from '@nhuthuynh.phu/yum-yum-common/dist/category/model.js';
import { SupplierModel } from '@nhuthuynh.phu/yum-yum-common/dist/supplier/model.js';
import { redisClient } from '../index.js';
import { clearCache } from '../utils/clearCache.js';

export const getSuppliers = async (req, res) => {
  try {
    const suppliers = await SupplierModel.find().sort({ createdAt: -1 });
    const result = { success: true, suppliers };
    await redisClient.set(req.originalUrl, JSON.stringify(result), {
      EX: 60 * 60 * 24 * 7,
    });
    res.status(200).json(result);
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

export const createSupplier = async (req, res) => {
  const { name, phone, address } = req.body;
  if (!name || !phone || !address)
    return res.status(400).json({
      success: false,
      message: 'Vui lòng điền đầy đủ thông tin',
    });

  try {
    const supplierCodeExisted = await SupplierModel.findOne({ name });
    if (supplierCodeExisted)
      return res
        .status(400)
        .json({ success: false, message: 'Tên Nhà cung cấp đã tồn tại' });

    const newSupplier = req.body;
    let supplier = new SupplierModel(newSupplier);
    await supplier.save();
    await clearCache('/suppliers');
    res.status(200).json({
      success: true,
      message: 'Tạo Nhà cung cấp mới thành công!',
      supplier,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

export const updateSupplier = async (req, res) => {
  const { name, phone, address } = req.body;
  if (!name || !phone || !address)
    return res.status(400).json({
      success: false,
      message: 'Vui lòng điền đầy đủ thông tin',
    });

  try {
    const supplierCodeExisted = await SupplierModel.findOne({
      _id: { $ne: req.params.id },
      name,
    });
    if (supplierCodeExisted)
      return res
        .status(400)
        .json({ success: false, message: 'Tên Nhà cung cấp đã tồn tại' });

    const updateSupplier = req.body;
    const supplier = await SupplierModel.findOneAndUpdate(
      { _id: req.params.id },
      updateSupplier,
      { new: true }
    );

    if (!supplier)
      return res
        .status(404)
        .json({ success: false, message: 'Không tìm thấy Nhà cung cấp' });
    await clearCache('/suppliers');
    res.status(200).json({
      success: true,
      message: 'Cập nhật Nhà cung cấp thành công!',
      supplier,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};

export const deleteSupplier = async (req, res) => {
  try {
    const category = await CategoryModel.findOne({ supplierId: req.params.id });
    if (category)
      return res.status(400).json({
        success: false,
        message: 'Không thể xoá vì đã tồn tại loại sản phẩm',
      });

    const supplier = await SupplierModel.findOneAndDelete({
      _id: req.params.id,
    });
    if (!supplier)
      return res
        .status(404)
        .json({ success: false, message: 'Không tìm thấy Nhà cung cấp' });
    await clearCache('/suppliers');
    res
      .status(200)
      .json({ success: true, message: 'Xoá thành công!', supplier });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, message: 'Lỗi máy chủ' });
  }
};
