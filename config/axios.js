import axios from 'axios';

const setAuthToken = () => {
  axios.defaults.headers.common['x-yumyum-key'] = `${process.env.X_YUMYUM_KEY}`;
};

export default setAuthToken;
