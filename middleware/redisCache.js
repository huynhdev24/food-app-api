import { redisClient } from '../index.js';

const redisCache = async (req, res, next) => {
  const data = await redisClient.get(req.originalUrl);
  if (data) {
    return res.status(200).json(JSON.parse(data));
  }
  next();
};

export default redisCache;
