export const apiNotificationUrl =
  process.env.NODE_ENV !== 'production'
    ? 'http://localhost:5051'
    : 'https://yum-yum-notification.onrender.com';

export const apiChatUrl =
  process.env.NODE_ENV !== 'production'
    ? 'http://localhost:5052'
    : 'https://yum-yum-chat.onrender.com';
